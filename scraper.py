import datetime
import requests
import argparse
from lxml import html


class FlightTable:
    def __init__(self):
        self.name = ''
        self.headers = []
        self.data = []

    def __str__(self):
        string = self.name + ': \n'
        for row in self.data:
            for h in self.headers:
                string += ' ' * 4 + h + ': ' + str(row[h]) + '\n'
            string += '\n'
        return string


def ask_for_info(request, debug_with_fiddler=False):
    session = requests.session()
    if debug_with_fiddler:
        proxies = {'http': 'http://127.0.0.1:8888', 'https': 'http://127.0.0.1:8888'}
        session.proxies = proxies
        session.verify = False

    url = 'https://www.flyniki.com/en/booking/flight/vacancy.php'
    r = session.get(url, allow_redirects=False)
    location = r.headers['Location']
    sid = {'sid': location[location.find('?') + len('sid=') + 1:]}

    headers = {'Content-Type': 'application/x-www-form-urlencoded'}

    response = session.post(url, params=sid, data=request, headers=headers)

    html_resp = None
    if response.status_code == 200:
        try:
            html_resp = response.json()['templates']['main']
        except KeyError:
            print(response.text)
    return html_resp


def get_flight_info(departure_code, destination_code, outbound_date, return_date, passengers):
    template = '_ajax[templates][]=main&' \
               '_ajax[templates][]=priceoverview&' \
               '_ajax[templates][]=infos&' \
               '_ajax[templates][]=flightinfo&' \
               '_ajax[requestParams][departure]={departure}&' \
               '_ajax[requestParams][destination]={destination}&' \
               '_ajax[requestParams][outboundDate]={out_date}&' \
               '_ajax[requestParams][adultCount]={adults}&' \
               '_ajax[requestParams][childCount]={childs}&' \
               '_ajax[requestParams][infantCount]={infants}&'

    request_body = template.format(departure=departure_code,
                                   destination=destination_code, out_date=outbound_date, **passengers)

    if return_date:
        request_body += '_ajax[requestParams][returnDate]={}&'.format(return_date)
    else:
        request_body += '_ajax[requestParams][oneway]=1&'
    html_resp = ask_for_info(request_body)
    if not html_resp:
        raise Exception('no response')
    return parse_response(html_resp)


def parse_response(html_resp, is_file_name=False):
    parse = [html.fromstring, html.parse][is_file_name]
    htree = parse(html_resp)

    tables = htree.xpath('//table[@class="flighttable"]')

    tables_headers = ['outbound', 'return']

    tables_list = []
    for table, th in zip(tables, tables_headers):
        flight_tbl = FlightTable()
        flight_tbl.name = th

        header_row = table.xpath('.//thead/tr[th[contains(@id,"head")]]/th')
        for header in header_row:
            h = ' '.join(header.text_content().strip().split())
            flight_tbl.headers.append(h)

        seen = set()
        seen_add = seen.add
        flight_tbl.headers = [x for x in flight_tbl.headers if not (x in seen or seen_add(x))]

        table_data = []
        flight_rows = table.xpath('.//tbody/tr[contains(@class, "flightrow")]')
        for row in flight_rows:
            row_dict = {}
            cells = row.xpath('.//td')
            for c, h in zip(cells, flight_tbl.headers):
                content = ' '.join(c.text_content().strip().split())
                if h or content:
                    if h:
                        row_dict[h] = content
            s = row_dict[flight_tbl.headers[-1]]
            s = s[:s.find(' ')]  # lowest price
            row_dict[flight_tbl.headers[-1]] = s
            table_data.append(row_dict)
        if (table is not None) and (th is not None):
            flight_tbl.data = table_data
        flight_tbl.headers = [h for h in flight_tbl.headers if h]
        tables_list.append(flight_tbl)
    return tables_list


def print_flight_info(flight_info):
    if not flight_info:
        print('there is no flights respective entered data!')

    for tbl in flight_info:
        print(tbl)


def print_iata():
    exit()


def cmd_input():
    parser = argparse.ArgumentParser()
    parser.add_argument('-codes', help='display IATA codes and exit', action='store_true',)

    parser.add_argument('departure', nargs='?', help='IATA code of departure airport')
    parser.add_argument('destination', nargs='?', help='IATA code of destination airport')

    today = datetime.datetime.now().strftime("%Y-%m-%d")
    parser.add_argument('-outbound_date', type=str, default=today, help='outbound date: yyyy-mm-dd [today, if passed]')
    parser.add_argument('-return_date', type=str, help='return date: yyyy-mm-dd [oneway flight, if passed]')

    parser.add_argument('-adults', default=1, help='number of adults [1, if passed]')
    parser.add_argument('-childs', default=0, help='number of childs [0, if passed]')
    parser.add_argument('-infants', default=0, help='number of infants [0, if passed]')

    args = parser.parse_args(['DME', 'DXB', '-outbound_date=2016-08-08', '-return_date=2016-08-25'])
    if args.codes:
        print_iata()
        exit()

    if not (args.destination and args.departure):
        print('error: some positional argumets are required (-h for help)')
        exit()

    passengers = {'adults': args.adults, 'childs': args.childs, 'infants': args.infants}
    return args.departure, args.destination, args.outbound_date, args.return_date, passengers


def main():
    entered_flight_info = cmd_input()
    requested_flight_info = get_flight_info(*entered_flight_info)
    print_flight_info(requested_flight_info)


if __name__ == '__main__':
    main()
